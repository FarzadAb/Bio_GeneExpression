from keras.layers import containers, AutoEncoder, Dense
from keras import models
import numpy as np
import random
import pickle
import time
from read import readData

numTests = 10
weightsFile = 'autoencoder_classification_weights.pickle'

X, _ = readData()
split = max( int(len(X) * .7), len(X) - numTests )
X_train = np.array(X[:split])
X_test = np.array(X[split:])

autoencoder = AutoEncoder(
    encoder = containers.Sequential([
        Dense(output_dim=300, input_dim=1000, activation='sigmoid'),
        Dense(100),
        Dense(30)
    ]),
    decoder = containers.Sequential([
        Dense(output_dim=100, input_dim=30),
        Dense(300),
        Dense(1000),
    ]),
    output_reconstruction=True
)

model = models.Sequential()
model.add(autoencoder)
model.compile(optimizer='sgd', loss='mse')

autoencoder.set_weights(pickle.load(open(weightsFile, 'r')))

enc_model = models.Sequential()
enc_model.add(autoencoder.encoder)
enc_model.compile(optimizer='sgd', loss='mse')

pickle.dump(enc_model.predict(X), open('genes_encoded.pickle', 'w'))

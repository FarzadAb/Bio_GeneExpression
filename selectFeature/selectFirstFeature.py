#!/usr/bin/python2
from keras.layers import Input, Dense
from keras.models import Model
import numpy as np
from represent import showOneOfK, printWithBorders
from read import readData

batch_size = 100
nb_epoch = 10

X, y = readData()
numClasses = y.shape[1]

split = int(len(X) // 10 * .8) * 10
y_train = y[:split]
y_test = y[split:]

errors = []
for i in range(X.shape[1] * X.shape[1]):
    if i == 100:
        print("reached 100")
    if i%500 == 0:
        print("reached %d" % i)
    x_train = X[:split,[i/X.shape[1],i%X.shape[1]]]
    x_test  = X[split:,[i/X.shape[1],i%X.shape[1]]]
    inp = Input(shape=(2,))
    l = Dense(10, activation='sigmoid')(inp)
    l = Dense(numClasses, activation='softmax')(l)
    model = Model(inp, l)
    model.compile(optimizer='sgd', loss='categorical_crossentropy')
    model.fit(x_train, y_train,
              shuffle=True,
              verbose=False,
              nb_epoch=nb_epoch,
              batch_size=batch_size,
              validation_data=(x_test, y_test))
    errors.append((model.evaluate(x_test, y_test, verbose=False), i))

print(sorted(errors)[:10])
# rep = model.predict(x_test)
# hist = [0 for i in range(numClasses)]
# for j in range(len(x_test)):
#     hist[showOneOfK(y_test[j], rep[j])] += 1

# printWithBorders('right call, place: ' + str(hist), down=True)
#
# precision = np.zeros((numClasses, numClasses))
# realClass = np.zeros(numClasses)
# rep = model.predict(x_test)
# for i in range(len(y_test)):
#     rc = y_test[i].tolist().index(1)
#     realClass[rc] += 1
#     precision[rc] += rep[i]
#
# for i, l in enumerate(precision):
#     print ', '.join( map(lambda x: '%.2f' % (x / realClass[i]), l) )

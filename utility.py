def partitionListTuple(l):
    l1, l2 = [], []
    for t in l:
        l1.append(t[0])
        l2.append(t[1])
    return l1, l2

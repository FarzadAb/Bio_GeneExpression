from BaseTraining import BaseTraining

# BaseTraining('3Layers', ['sigmoid', 'sigmoid'], [1000, 200, 30])
BaseTraining('softmaxActivation', ['softmax', 'softmax'], [1000, 200, 30])
BaseTraining('tanhActivation', ['tanh', 'tanh'], [1000, 200, 30])
BaseTraining('softplusActivation', ['softplus', 'softplus'], [1000, 200, 30])
BaseTraining('softsignActivation', ['softsign', 'softsign'], [1000, 200, 30])
BaseTraining('reluActivation', ['relu', 'relu'], [1000, 200, 30])
BaseTraining('hard_sigmoidActivation', ['hard_sigmoid', 'hard_sigmoid'], [1000, 200, 30])

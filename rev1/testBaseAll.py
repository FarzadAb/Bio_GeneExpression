from BaseTraining import BaseTraining

# BaseTraining('fully_linear', [])
BaseTraining('1LSigmoid', ['sigmoid'])
BaseTraining('2LSigmoid', ['sigmoid', 'sigmoid'])
BaseTraining('3LSigmoid', ['sigmoid', 'sigmoid', 'sigmoid'])
BaseTraining('2farLSigmoid', ['sigmoid', 'linear', 'linear', 'sigmoid'])

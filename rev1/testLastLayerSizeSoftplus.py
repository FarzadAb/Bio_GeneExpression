from BaseTraining import BaseTraining

BaseTraining('encWith15(softplus)', ['softplus', 'softplus'], dims=[1000, 100, 15])
BaseTraining('encWith20(softplus)', ['softplus', 'softplus'], dims=[1000, 100, 20])
BaseTraining('encWith25(softplus)', ['softplus', 'softplus'], dims=[1000, 100, 25])
BaseTraining('encWith30(softplus)', ['softplus', 'softplus'], dims=[1000, 100, 30])
BaseTraining('encWith35(softplus)', ['softplus', 'softplus'], dims=[1000, 100, 35])
BaseTraining('encWith40(softplus)', ['softplus', 'softplus'], dims=[1000, 100, 40])

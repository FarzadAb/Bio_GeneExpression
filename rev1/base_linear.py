import pickle
import numpy as np
from read import readData
from NeuralNets import AE, Classifier
from represent import show

weightsFile = 'base_linear_ae_w.pickle'

X, y = readData()
split = int(len(X) * .80)
X_train = np.array(X[:split])
y_train = np.array(y[:split])
X_test = np.array(X[split:])
y_test = np.array(y[split:])
numClasses = y.shape[1]

print len(X), len(X_train)

ae = AE([1000, 300, 100, 30], weightsFile, activations=['sigmoid'], optimizer='sgd')

ae.autoTrain(X_train, X_test, n_rounds=10, n_epochs=100, batch_size=200)
ae.autoTrain(X_train, X_test, n_rounds=5, n_epochs=200)
ae.autoTrain(X_train, X_test, n_rounds=8, n_epochs=500, batch_size=None)

ae.evaluate(X_test, X_test)
rep = ae.predict(X_test)
for i in range(10):
    show(X_test[i], rep[i])

X_enc = ae.encode(X)

X_train = np.array(X_enc[:split])
X_test = np.array(X_enc[split:])

weightsFile = 'base_linear_cls_w.pickle'

m2 = Classifier([ae.dims[0], 40, numClasses], weightsFile, doRead=False)
m2.autoTrain(X_train, y_train, X_test, y_test, n_epochs=100, n_rounds=10)
m2.autoTrain(X_train, y_train, X_test, y_test, n_epochs=1000, n_rounds=4)
m2.presentTests(X_test, y_test)

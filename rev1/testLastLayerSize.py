from BaseTraining import BaseTraining

BaseTraining('encWith10', ['sigmoid', 'sigmoid'], dims=[1000, 100, 10])
BaseTraining('encWith20', ['sigmoid', 'sigmoid'], dims=[1000, 100, 20])
BaseTraining('encWith50', ['sigmoid', 'sigmoid'], dims=[1000, 100, 50])
BaseTraining('encWith60', ['sigmoid', 'sigmoid'], dims=[1000, 100, 60])
BaseTraining('encWith80', ['sigmoid', 'sigmoid'], dims=[1000, 100, 80])

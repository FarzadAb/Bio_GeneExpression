from keras.layers import containers, AutoEncoder, Dense
from keras import models
import numpy as np
import random
import pickle
import time
import copy
from read import readData
from represent import showOneOfK, printWithBorders
from matplotlib import pylab

def trainModel(model, X_train, Y_train, X_test=None, Y_test=None, n_rounds=100, n_epochs=10, batch_size=50, verbosity=3):
    if n_rounds < 1:
        return

    loss = []

    train_start = time.time()
    for i in range(n_rounds):
        if verbosity >= 2:
            print('*' * 30)
            print('Starting round %d ...' %(i+1))
        start = time.time()
        if batch_size:
            model.fit(X_train, Y_train, batch_size=batch_size, nb_epoch=n_epochs, verbose=0)
        else:
            model.fit(X_train, Y_train, nb_epoch=n_epochs, verbose=0)
        if model.doWrite:
            pickle.dump(model.get_weights(), open(model.weightsFile, 'w'))
        loss.append(model.evaluate(X_test, Y_test, show_accuracy=False, verbose=0))
        if verbosity >= 2:
            print('this round took %.1f seconds' % (time.time() - start))
            if verbosity == 2:
                print(model.evaluate(X_test, Y_test, show_accuracy=True, verbose=0))
            else:
                print(loss)

    if verbosity >= 1:
        print('\n')
        print('##' * 30)
        print('training done in %.1f seconds' % (time.time() - train_start))
        print('the last MSE was: %.4f' % loss[-1])
        print('##' * 30)


class Model(object):
    def __init__(self, dims, weightsFile, doRead=True, doWrite=True, optimizer='sgd'):
        self.dims = dims
        self.weightsFile = weightsFile
        self.doRead = doRead
        self.doWrite = doWrite
        self.optimizer = optimizer

    def fit(self, *args, **kwargs):
        return self.model.fit(*args, **kwargs)

    def evaluate(self, *args, **kwargs):
        return self.model.evaluate(*args, **kwargs)

    def get_weights(self, *args, **kwargs):
        return self.model.get_weights(*args, **kwargs)

    def predict(self, X, *args, **kwargs):
        return self.model.predict(X, *args, **kwargs)


class Classifier(Model):
    def __init__(self, dims, weightsFile, doRead=True, doWrite=True, optimizer='sgd'):
        super(Classifier, self).__init__(dims, weightsFile, doRead, doWrite, optimizer)
        self.model = models.Sequential()
        self.numClasses = dims[-1]
        for i in range(len(dims)-1):
            if i == len(dims)-2:
                self.model.add(Dense(output_dim=dims[i+1], input_dim=dims[i], activation='softmax'))
            else:
                self.model.add(Dense(output_dim=dims[i+1], input_dim=dims[i], activation='sigmoid'))
        self.model.compile(optimizer=optimizer, loss='categorical_crossentropy')

        if doRead:
            try:
                self.model.set_weights(pickle.load(open(weightsFile, 'r')))
                print('data read completely')
            except:
                print('Could not read weights file, starting out with nothing')

    def autoTrain(self, *args, **kwargs):
        trainModel(self, *args, **kwargs)

    def changeOptimizer(self, optimizer):
        self.model.compile(optimizer=optimizer, loss='categorical_crossentropy')

    def presentTests(self, X_test, y_test, verbose=True):
        rep = self.predict(X_test)
        hist = [0 for i in range(self.numClasses)]
        for j in range(len(X_test)):
            hist[showOneOfK(y_test[j], rep[j], verbose)] += 1

        printWithBorders('right call, place: ' + str(hist), down=True)

        precision = np.zeros((self.numClasses, self.numClasses))
        realClass = np.zeros(self.numClasses)
        rep = self.model.predict(X_test)
        for i in range(len(y_test)):
            rc = y_test[i].tolist().index(1)
            realClass[rc] += 1
            precision[rc] += rep[i]

        for i, l in enumerate(precision):
            print ', '.join( map(lambda x: '%.2f' % (x / realClass[i]), l) )


class AE(Model):
    def __init__(self, dims, weightsFile, doRead=True, doWrite=True, optimizer='sgd', activations=None):
        activations = copy.copy(activations)
        super(AE, self).__init__(dims, weightsFile, doRead, doWrite, optimizer)
        if activations == None:
            activations = ['sigmoid'] * (2*len(dims)-3)
        encoder = []
        for i in range(len(dims)-1):
            if len(activations):
                act = activations.pop()
            else:
                act = 'linear'
            encoder.append(
                Dense(output_dim=dims[i+1], input_dim=dims[i], activation=act)
            )

        decoder = []
        for i in range(len(dims)-2):
            if len(activations):
                act = activations.pop()
            else:
                act = 'linear'
            decoder.append(
                Dense(output_dim=dims[-i-2], input_dim=dims[-i-1], activation=act)
            )
        decoder.append(Dense(output_dim=dims[0], input_dim=dims[1]))

        self.autoencoder = AutoEncoder(
            encoder = containers.Sequential(encoder),
            decoder = containers.Sequential(decoder),
            output_reconstruction=True
        )

        self.model = models.Sequential()
        self.model.add(self.autoencoder)
        self.model.compile(optimizer=optimizer, loss='mse')

        if doRead:
            try:
                self.autoencoder.set_weights(pickle.load(open(weightsFile, 'r')))
                print('data read completely')
            except:
                print('Could not read weights file, starting out with nothing')

    def autoTrain(self, X_train, X_test=None, *args, **kwargs):
        trainModel(self, X_train, X_train, X_test, X_test, *args, **kwargs)

    def changeOptimizer(self, optimizer):
        self.model.compile(optimizer=optimizer, loss='mse')

    def encode(self, X, fileToWrite=None):
        enc_model = models.Sequential()
        enc_model.add(self.autoencoder.encoder)
        enc_model.compile(optimizer='sgd', loss='mse')

        pred = enc_model.predict(X)

        if isinstance(fileToWrite, str):
            pickle.dump(pred, open(fileToWrite, 'w'))
            print('encoded data dumped to ' + fileToWrite)

        return pred

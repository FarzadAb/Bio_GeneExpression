import numpy as np
import pickle
import random

seed = 716523025775356
random.seed(seed)

def oneOfK(i, k):
    return [(i==j)*1 for j in range(k)]

def oneOfKLbls(val, lbls):
    return oneOfK(lbls[val], len(lbls))

def readLabels(labelFile='SamplesCellTypes.txt', doReadClassNames=False):
    data = open('data/' + labelFile, 'r').readlines()
    labels = {l.split(None,1)[0]: l.split(None,1)[1] for l in data[1:]}
    enumeratedLabels = list(enumerate(set(labels.values())))
    lblNums = {lbl: i for i, lbl in enumeratedLabels}
    out = {lbl: oneOfKLbls(labels[lbl], lblNums) for lbl in labels}
    if doReadClassNames:
        return out, {t[1][:-1]: t[0] for t in enumeratedLabels}
    return out

def readData(
        dataFile='data1000genes.txt',
        labelFile='SamplesCellTypes.txt',
        doReadLabels=True,
        doReadGeneNames=False,
        doReadClassNames=False):
    data = open('data/' + dataFile, 'r').readlines()
    samples = data[0].split()
    gene_names = [data[i].split()[0] for i in range(1, len(data))]
    Xt = map(lambda x: map(float, x.split()[1:]), data[1:])
    X = np.array(Xt).transpose()

    reorder = range(len(X))
    random.shuffle(reorder)
    X = np.array([X[reorder[i]] for i in range(len(X))])
    out = [X]
    if doReadLabels:
        if doReadClassNames:
            lbls, cNames = readLabels(labelFile, doReadClassNames=True)
        else:
            lbls = readLabels(labelFile)
        y = np.array([lbls[samples[reorder[i]]] for i in range(len(samples))])
        out.append(y)
        if doReadClassNames:
            out.append(cNames)
    if doReadGeneNames:
        out.append(gene_names)
    return out

def readEncData(encDataFile='genes_encoded.pickle', dataFile='data1000genes.txt', labelFile='SamplesCellTypes.txt'):
    samples = open('data/' + dataFile, 'r').readline().split()
    encData = pickle.load(open('data/' + encDataFile, 'r'))
    reorder = range(len(encData))
    random.shuffle(reorder)
    encData = np.array([encData[reorder[i]] for i in range(len(encData))])
    lbls = readLabels(labelFile)
    y = np.array([lbls[samples[reorder[i]]] for i in range(len(samples))])
    return encData, y

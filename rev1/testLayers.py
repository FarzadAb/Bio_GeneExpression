from BaseTraining import BaseTraining

# BaseTraining('4Layers', ['sigmoid', 'sigmoid'])
BaseTraining('2Layers', ['sigmoid', 'sigmoid'], [1000, 30])
BaseTraining('3Layers', ['sigmoid', 'sigmoid'], [1000, 200, 30])
BaseTraining('5Layers', ['sigmoid', 'sigmoid'], [1000, 500, 250, 100, 30])

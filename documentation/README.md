# How to setup environment and  run Codes
This project is hosted by *gitlab* in [https://gitlab.com/FarzadAb/Bio_GeneExpression].

Also the codes are available in the *damavand* server under `/s/chopin/a/grad/asharifi/e/Farzad/Bio_GeneExpression`

To setup the environment you can use the command `source Env`. Then you can use the `Code` command to run a module in your code with the required configurations. For example, to run the `testRandom.py` file in the `rev2` folder you can run:
    source Env
    Code rev2.testRandom
and it will pull the code from gitlab and run the last available version. The output will be recorded in a file named `rev2.testRandom.out` and the weights file (which you might not even need) should be stored in the `weights` folder, also the output will be shown in the monitor until you `ctrl+C` which would not interrupt the running program. Additionally by convention, data should be put in the `data` folder, for example, the file `data1000genes.txt` is currently stored in this folder in the root of the project in the damavand server.

# Code structure
The code is a little messy right now, so sorry about that. There are a bunch of code pieces scattered around in the root of the project, but there are two clean implementations available in the `rev1` and `rev2` folder. They have copy & pasted versions of the essential parts of some parts of the codes. Also these contain some kinds of experiments to figure out the best parameters for the algorithm (like the best activation layers, best layer structure, and so on). The results are available in the `results` folder (if you have trouble with the file types contact me).

I really haven't had the time to refactor the code, also because I didn't want to make changes in a way that the old parts wouldn't work, almost all of time, the result might be a little messy and hard to understand.

In the next part, some parts of the code will be explained one by one:

## read.py
This module is responsible for reading the data and transforming it into a workable *numpy* object. Almost all of the reading goes through the `readData` function. It's output might be a little weird. it returns a list, and in this list can contain all sorts of data. The first element is always the actual data, but other things can be added by setting the `doRead*` arguments. for example by default the `doReadLabels` is turned on, therefore the second element is the labels vector. The usual way of using this function is:
```
from rev2.read import readData
X, y = readData()
```
after running this, `X` will be a numpy array with *number of samples* rows and *number of gene expressions* columns. By the default, this function reads from `data1000genes.txt`, therefore has 1040 samples and 1000 genes. You can also get the gene names by passing `doReadGeneNames=True` or get the class-names (cell-types) by passing `doReadClassNames=True`. Furthermore, the variable `y` will contain a vector of length 16 for each sample, with all zeros except only one `1` which specifies the sample's class (cell-type).


## represent.py
This module contains some utility functions to represent the data and results. For example the `show` function will compare the gene expressions for a single sample with the predictions for the same sample, therefore it takes two vectors of the same size. Then it shows for each column, the *actual* and the *fitted* values and uses colors to indicate the closeness (this part might need a little work). Also if the column-size is too large, it samples both vectors and only shows `max_length` columns.

The `showOneOfK` function is similiar but it is used to compare the results of the last layer of classification network, which consist of a vector the size of the label (16 cell-types) and numbers in which describes the predicted percentage of the sample being in that specific category (sum of the vector should be `1`).

Finally `printWithBorders` function is only used to print a string in a rectangular border. You can specify which edges (sides) should be visible (by default, it only prints a line above the string).

## NeuralNets.py
This module contains an implementation of an `Auto Encoder` and a `Classifier` which use an old version of *keras* library. They are designed to be a little flexible. Also they make it easier to run the tests and unify the way the results are presented in the output.

Both `AE` and `Classifier` inherit from `Model` and have the function `autoTrain`. This function uses a unified training method. You should specify `dims` and `weightsFile` to initialize this classes. `dims` represents the number of nodes in each layer of the neural net. In the case of `AE`, this layering will be copied and reversed to specify the encoding layers (i.e. [100, 30] to [100, 30, 100]). `weightsFile` is the name of the file where the learned weights will be stored.

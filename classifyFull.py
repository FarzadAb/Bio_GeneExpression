# run with:
# source ~/e/Farzad/myEnv/bin/activate
# nohup python classifyFull.py > classification.out &
from keras.layers import containers, AutoEncoder, Dense
from keras import models
import numpy as np
import random
import pickle
import time
from represent import show, showOneOfK, printWithBorders
from read import readData, oneOfK

def bucketSortByTypes(data, numClasses):
    ret = [[] for _ in range(0, numClasses)]
    for d in data:
        ret[d[1]].append(d)
    return ret

def flatten(l):
    ret = []
    for item in l:
        ret.extend(item)
    return ret

def partition(l, ind):
    return (l[:ind], l[ind:])

def flatPartition(l):
    return map(lambda t: t[0], l), map(lambda t: t[1], l)

#################################
# training deprived autoencoder #
#################################

deprivedWeightsFile = 'autoencoder_weights_depriving.pickle'

X, y = readData()
removeClass = [13]
X = np.array(map(lambda tup: tup[0],
                filter(lambda tup: list(tup[1]).index(1) not in removeClass,
                    zip(X,y)
                )
            ))

split = max(int(len(X) * .7), len(X) - 100)
X_train = X[:split]
X_test = X[split:]

print('len of train set is : ' + str(len(X_train)) )

autoencoder = AutoEncoder(
    encoder = containers.Sequential([
        Dense(output_dim=300, input_dim=1000, activation='sigmoid'),
        Dense(100),
        Dense(30)
    ]),
    decoder = containers.Sequential([
        Dense(output_dim=100, input_dim=30),
        Dense(300),
        Dense(1000),
    ]),
    output_reconstruction=True
)

model = models.Sequential()
model.add(autoencoder)
model.compile(optimizer='sgd', loss='mse')

loss = []

for i in range(300):
    print('*' * 30)
    print('Starting round %d ...' %(i+1))
    start = time.time()
    model.fit(X_train, X_train, nb_epoch=50, verbose=0)
    pickle.dump(autoencoder.get_weights(), open(deprivedWeightsFile, 'w'))
    print('this round took %.1f seconds' % (time.time() - start))
    loss.append(model.evaluate(X_test, X_test, show_accuracy=False))
    print(model.evaluate(X_test, X_test, show_accuracy=True))
    print(loss)

f = open('loss_autoencoder_deprinving.txt', 'w')
f.write(str(loss))
f.close()

#############################################
# reading data and taking balanced test set #
#############################################
testPercent = .8

X, y = readData()
y = map(lambda t: list(t).index(1), y)
numClasses = max(y)+1

train, test = flatPartition(
            map(
                lambda l: partition(l, int(len(l)*testPercent)),
                bucketSortByTypes(
                    zip(
                        X,
                        y
                    ),
                    numClasses
                )
            )
        )

X_train, y_train = flatPartition(flatten(train))
X_train = np.array(X_train)
y_train = np.array(map(lambda t: oneOfK(t, numClasses), y_train))

X_test, y_test = flatPartition(flatten(test))
X_test = np.array(X_test)
y_test = np.array(map(lambda t: oneOfK(t, numClasses), y_test))

print('len of train set is : ' + str(len(X_train)) )

###############################################
# training the autoencoder for classification #
###############################################

aeClassifyWeightsFile = 'autoencoder_classification_weights.pickle'

autoencoder = AutoEncoder(
    encoder = containers.Sequential([
        Dense(output_dim=300, input_dim=1000, activation='sigmoid'),
        Dense(100),
        Dense(30)
    ]),
    decoder = containers.Sequential([
        Dense(output_dim=100, input_dim=30),
        Dense(300),
        Dense(1000),
    ]),
    output_reconstruction=True
)

ae_model = models.Sequential()
ae_model.add(autoencoder)
ae_model.compile(optimizer='sgd', loss='mse')

# try:
#     autoencoder.set_weights(pickle.load(open(aeClassifyWeightsFile, 'r')))
# except:
#     pass


ae_loss = []

for i in range(300):
    print('*' * 30)
    print('Starting round %d ...' %(i+1))
    start = time.time()
    ae_model.fit(X_train, X_train, nb_epoch=50, verbose=0)
    pickle.dump(autoencoder.get_weights(), open(aeClassifyWeightsFile, 'w'))
    print('this round took %.1f seconds' % (time.time() - start))
    ae_loss.append(ae_model.evaluate(X_test, X_test, show_accuracy=False))
    print(ae_model.evaluate(X_test, X_test, show_accuracy=True))
    # print(ae_loss)

print('ae_loss: ' + str(ae_loss))
f = open('loss_autoencoder_classification.txt', 'w')
f.write(str(ae_loss))
f.close()

###############################
# now training the classifier #
###############################

enc_model = models.Sequential()
enc_model.add(autoencoder.encoder)
enc_model.compile(optimizer='sgd', loss='mse')

enc_X_train = enc_model.predict(X_train)
enc_X_test  = enc_model.predict(X_test)

m2ClassifyWeightsFile = 'm2_classification_weights.pickle'

m2_model = models.Sequential()
m2_model.add(Dense(output_dim=40, input_dim=30, activation='sigmoid'))
m2_model.add(Dense(numClasses, activation='softmax'))
m2_model.compile(optimizer='sgd', loss='categorical_crossentropy')

m2_loss = []

for i in range(100):
    print('*' * 30)
    print('Starting round %d ...' %(i+1))
    start = time.time()
    m2_model.fit(enc_X_train, y_train, nb_epoch=50, verbose=0)
    pickle.dump(m2_model.get_weights(), open(m2ClassifyWeightsFile, 'w'))
    print('this round took %.1f seconds' % (time.time() - start))
    rep = m2_model.predict(enc_X_test)
    m2_loss.append(m2_model.evaluate(enc_X_test, y_test))

print('m2_loss: ' + str(m2_loss))
f = open('loss_m2_classification.txt', 'w')
f.write(str(m2_loss))
f.close()

#############################################
# now testing with the deprived autoencoder #
#############################################

deprivedWeightsFile = 'autoencoder_weights_depriving.pickle'

dp_ae = AutoEncoder(
    encoder = containers.Sequential([
        Dense(output_dim=300, input_dim=1000, activation='sigmoid'),
        Dense(100),
        Dense(30)
    ]),
    decoder = containers.Sequential([
        Dense(output_dim=100, input_dim=30),
        Dense(300),
        Dense(1000),
    ]),
    output_reconstruction=True
)

dp_ae_model = models.Sequential()
dp_ae_model.add(dp_ae)
dp_ae_model.compile(optimizer='sgd', loss='mse')

dp_ae.set_weights(pickle.load(open(deprivedWeightsFile, 'r')))

X_test_dp_ae = enc_model.predict(dp_ae_model.predict(X_test))
print('loss after all has passed is: ' + \
    str(m2_model.evaluate(X_test_dp_ae, y_test))
)

rep = m2_model.predict(X_test_dp_ae)
hist = [0 for i in range(numClasses)]
for j in range(len(X_test)):
    hist[showOneOfK(y_test[j], rep[j])] += 1

printWithBorders('right call, place: ' + str(hist), down=True)

precision = np.zeros((numClasses, numClasses))
realClass = np.zeros(numClasses)
for i in range(len(y_test)):
    # print y_test[i], ' to ', y_test[i].tolist().index(1)
    rc = y_test[i].tolist().index(1)
    realClass[rc] += 1
    precision[rc] += rep[i]

for i, l in enumerate(precision):
    print ', '.join( map(lambda x: '%.2f' % (x / realClass[i]), l) )

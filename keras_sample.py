from keras.layers import containers, AutoEncoder, Dense
from keras import models
import numpy as np
# from transform import linear_transform as transform
from transform import transform
from represent import show

y_train = np.random.uniform(-1, 1, (5000, 3))
X_train = np.array(map(transform, y_train))

y_test = np.random.uniform(-1, 1, (10, 3))
X_test = np.array(map(transform, y_test))

autoencoder = AutoEncoder(
    encoder = containers.Sequential([
        Dense(output_dim=7, input_dim=10, activation='tanh'),
        Dense(3),
    ]),
    decoder = containers.Sequential([
        Dense(output_dim=7, input_dim=3),
        Dense(10),
    ]),
    output_reconstruction=True
)

model = models.Sequential()
model.add(autoencoder)
model.compile(optimizer='sgd', loss='mse')
model.fit(X_train, X_train, nb_epoch=500, verbose=0)

rep = model.predict(X_test)

for i in range(len(X_test)):
    show(X_test[i], rep[i])

# encoders = []
# decoders = []
# nb_hidden_layers = [10, 7, 3]
# X_train_tmp = np.copy(X_train)
#
# for i, (n_in, n_out) in enumerate(zip(nb_hidden_layers[:-1], nb_hidden_layers[1:]), start=1):
#     print('Training the layer {}: Input {} -> Output {}'.format(i, n_in, n_out))
#     encoder = containers.Sequential([
#         Dense(output_dim=n_out, input_dim=n_in, activation='tanh'),
#     ])
#     decoder = containers.Sequential([
#         Dense(output_dim=n_in, input_dim=n_out),
#     ])
#     ae = AutoEncoder(encoder=encoder, decoder=decoder, output_reconstruction=True)
#     model = models.Sequential()
#     model.add(ae)
#     model.compile(optimizer='sgd', loss='mse')
#     model.fit(X_train_tmp, X_train_tmp, nb_epoch=100, verbose=0)
#     encoders.append(model.layers[0].encoder)
#     decoders.append(model.layers[0].decoder)
#     ae.output_reconstruction = False
#     model.compile(optimizer='sgd', loss='mse')
#     X_train_tmp = model.predict(X_train_tmp)


# predicting compressed rep of inputs:
# autoencoder.output_reconstruction = False  # the model has to be recompiled after modifying this property
# model.compile(optimizer='sgd', loss='mse')

# the model is still trainable, although it now expects compressed rep as targets:
# model.fit(X_test, rep, nb_epoch=1)  # in this case the loss will be 0, so it's useless

# to keep training against the original inputs, just switch back output_reconstruction to True:
# autoencoder.output_reconstruction = True
# model.compile(optimizer='sgd', loss='mse')
# model.fit(X_train, X_train, nb_epoch=10)

from rev2.BaseTraining import BaseTraining
from rev2.read import readRandomPortionOfData, readBestVarColumns, readSampleTranscriptionFactors
from functools import partial


dims = [1000, 300, 100, 30]

for size in [50, 100, 200, 400, 800, 1000, 2000]:
    dims[0] = size
    BaseTraining('random%d_2LSigmoid' % size, ['sigmoid', 'sigmoid'],
                 readFunc=partial(readRandomPortionOfData, numColumns=size),
                 dims=dims)
    BaseTraining('bestVar%d_2LSigmoid' % size, ['sigmoid', 'sigmoid'],
                 readFunc=partial(readBestVarColumns, numColumns=size),
                 dims=dims)
    BaseTraining('transcriptFactor%d_2LSigmoid' % size, ['sigmoid', 'sigmoid'],
                 readFunc=partial(readSampleTranscriptionFactors, numColumns=size),
                 dims=dims)

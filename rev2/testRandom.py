from rev2.BaseTraining import BaseTraining
from rev2.read import readRandomPortionOfData

BaseTraining('random1_2LSigmoid', ['sigmoid', 'sigmoid'], readFunc=readRandomPortionOfData)
BaseTraining('random2_2LSigmoid', ['sigmoid', 'sigmoid'], readFunc=readRandomPortionOfData)
BaseTraining('random3_2LSigmoid', ['sigmoid', 'sigmoid'], readFunc=readRandomPortionOfData)
BaseTraining('random4_2LSigmoid', ['sigmoid', 'sigmoid'], readFunc=readRandomPortionOfData)

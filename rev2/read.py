import numpy as np
import pickle
import random
from functools import partial

seed = 716523025775356
random.seed(seed)


class Files(object):
    '''
        static file names are defined here
    '''
    prefix = 'data/'
    the1000Genes = 'data1000genes.txt'
    allGenes = 'CellNet_hgu133plus2_full.txt'
    labels = 'SamplesCellTypes.txt'
    encoded = 'genes_encoded.pickle'
    transcriptFactors = 'TFs.txt'


def oneOfK(i, k):
    return [(i==j)*1 for j in range(k)]


def oneOfKLbls(val, lbls):
    return oneOfK(lbls[val], len(lbls))


def readLabels(labelFile=Files.labels, doReadClassNames=False):
    data = open(Files.prefix + labelFile, 'r').readlines()
    labels = {l.split(None,1)[0]: l.split(None,1)[1] for l in data[1:]}
    enumeratedLabels = list(enumerate(set(labels.values())))
    lblNums = {lbl: i for i, lbl in enumeratedLabels}
    out = {lbl: oneOfKLbls(labels[lbl], lblNums) for lbl in labels}
    if doReadClassNames:
        return out, {t[1][:-1]: t[0] for t in enumeratedLabels}
    return out


def randomSpecialFunc(X, geneNames=None, outputSize=1000, verbose=True):
    variance = np.var(X, axis=0)
    valid_indices = np.array(range(X.shape[1]))[variance > 0.1]
    sampledCols = random.sample(valid_indices, outputSize)
    if verbose:
        print('%d Random Columns selected, sampled columns attrs:\n\tmin var:%f\n\tmax var:%f\n\tmean var:%f'
               % (len(sampledCols), variance[sampledCols].min(),
                  variance[sampledCols].max(), variance[sampledCols].mean())
        )

    return X[:, sampledCols]


def trascriptFactorSpecialFunc(X, geneNames, outputSize=1000, verbose=True):
    tfs = set(open(Files.prefix + Files.transcriptFactors, 'r').readline().replace(',',' ').split())
    tf_places = [name in tfs for name in geneNames]
    valid_indices = np.array(range(len(geneNames)))[np.array(tf_places)]
    sampledCols = random.sample(valid_indices, outputSize)
    if verbose:
        print('%d Transcription Factor Columns selected' % len(sampledCols))

    return X[:, sampledCols]


def bestVarSpecialFunc(X, geneNames=None, outputSize=1000, verbose=True):
    variance = np.var(X, axis=0)
    sortedPairs = sorted(zip(variance.tolist(), range(X.shape[1])), reverse=True)
    sampledCols = [v[1] for v in sortedPairs[:outputSize]]
    if verbose:
        print('%d BestVar Columns selected, sampled columns attrs:\n\tmin var:%f\n\tmax var:%f\n\tmean var:%f'
               % (len(sampledCols), variance[sampledCols].min(),
                  variance[sampledCols].max(), variance[sampledCols].mean())
        )

    return X[:, sampledCols]


def readData(
        dataFile=Files.the1000Genes,
        labelFile=Files.labels,
        doReadLabels=True,
        doReadGeneNames=False,
        doReadClassNames=False,
        specialFunc=None):
    data = open(Files.prefix + dataFile, 'r').readlines()
    samples = data[0].split()
    gene_names = [data[i].split()[0] for i in range(1, len(data))]
    Xt = map(lambda x: map(float, x.split()[1:]), data[1:])
    X = np.array(Xt).transpose()
    if not specialFunc is None:
        X = specialFunc(X, gene_names)

    reorder = range(len(X))
    random.shuffle(reorder)
    X = np.array([X[reorder[i]] for i in range(len(X))])
    out = [X]
    if doReadLabels:
        if doReadClassNames:
            lbls, cNames = readLabels(labelFile, doReadClassNames=True)
        else:
            lbls = readLabels(labelFile)
        y = np.array([lbls[samples[reorder[i]]] for i in range(len(samples))])
        out.append(y)
        if doReadClassNames:
            out.append(cNames)
    if doReadGeneNames:
        out.append(gene_names)
    return out


def readRandomPortionOfData(dataFile=Files.allGenes, numColumns=1000, *args, **kwargs):
    return readData(
        dataFile=dataFile,
        specialFunc=partial(randomSpecialFunc, outputSize=numColumns),
        *args, **kwargs)


def readBestVarColumns(dataFile=Files.allGenes, numColumns=1000, *args, **kwargs):
    return readData(
        dataFile=dataFile,
        specialFunc=partial(bestVarSpecialFunc, outputSize=numColumns),
        *args, **kwargs)


def readSampleTranscriptionFactors(dataFile=Files.allGenes, numColumns=1000, *args, **kwargs):
    return readData(
        dataFile=dataFile,
        specialFunc=partial(trascriptFactorSpecialFunc, outputSize=numColumns),
        *args, **kwargs)


def readEncData(encDataFile=Files.encoded, dataFile=Files.the1000Genes, labelFile=Files.labels):
    samples = open(Files.prefix + dataFile, 'r').readline().split()
    encData = pickle.load(open(Files.prefix + encDataFile, 'r'))
    reorder = range(len(encData))
    random.shuffle(reorder)
    encData = np.array([encData[reorder[i]] for i in range(len(encData))])
    lbls = readLabels(labelFile)
    y = np.array([lbls[samples[reorder[i]]] for i in range(len(samples))])
    return encData, y

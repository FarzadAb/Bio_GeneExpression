import pickle
import numpy as np
from rev2.read import readData
from rev2.NeuralNets import AE, Classifier
from rev2.represent import show, printWithBorders

class BaseTraining(object):

    def readData(self):
        self.X, self.y = self.readFunc()
        self.split = int(len(self.X) * .80)
        self.X_train = np.array(self.X[:self.split])
        self.y_train = np.array(self.y[:self.split])
        self.X_test = np.array(self.X[self.split:])
        self.y_test = np.array(self.y[self.split:])
        self.numClasses = self.y.shape[1]
        # print len(X), len(X_train)

    def train(self):
        self.ae.autoTrain(self.X_train, self.X_test, n_rounds=5, n_epochs=1000, batch_size=200, verbosity=False)
        self.ae.autoTrain(self.X_train, self.X_test, n_rounds=15, n_epochs=1000, verbosity=False)
        # self.ae.autoTrain(self.X_train, self.X_test, n_rounds=8, n_epochs=500, batch_size=None, verbosity=False)
        # self.ae.autoTrain(self.X_train, self.X_test, n_rounds=8, n_epochs=500, batch_size=None, verbosity=False)
        # self.ae.autoTrain(self.X_train, self.X_test, n_rounds=5, n_epochs=1000, verbosity=False)

    def __init__(self, name, activations, dims=[1000, 300, 100, 30], readFunc=readData):
        self.ae_wf = 'weights/' + name + '_base_ae.pickle'
        self.cls_wf = 'weights/' + name + '_base_cls.pickle'
        self.dims = dims
        self.activations = activations
        self.readFunc = readFunc

        self.readData()

        self.ae = AE(self.dims, self.ae_wf, doRead=False, activations=self.activations, optimizer='sgd')
        self.train()

        error = self.ae.evaluate(self.X_test, self.X_test, verbose=0)
        printWithBorders(
            ('trained autoencoder with activations=%s and dims=%s\n'
            + '\tgot error:%.4f') % ( str(activations), str(dims), error ),
            down=True, left=True, right=True
        )
        rep = self.ae.predict(self.X_test)
        for i in range(3):
            show(self.X_test[i], rep[i])

        X_enc = self.ae.encode(self.X)

        self.X_train_cls = np.array(X_enc[:self.split])
        self.X_test_cls = np.array(X_enc[self.split:])

        self.cls = Classifier([self.dims[-1], 40, self.numClasses], self.cls_wf, doRead=False)
        # self.cls.autoTrain(self.X_train_cls, self.y_train, self.X_test_cls, self.y_test, n_epochs=100, n_rounds=10, verbosity=False)
        self.cls.autoTrain(self.X_train_cls, self.y_train, self.X_test_cls, self.y_test, n_epochs=1000, n_rounds=20, verbosity=False)
        error = self.cls.evaluate(self.X_test_cls, self.y_test, verbose=0)
        printWithBorders(
            '\ttraining the classifier got error:%.5f' % ( error ),
            down=True, left=True, right=True
        )
        self.cls.presentTests(self.X_test_cls, self.y_test)

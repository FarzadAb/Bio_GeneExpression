from __future__ import print_function
import random, sys


def _to_string(x, good=None, length=7, decimal_points=2):
    reset = '\033[00m'
    red = '\033[01;31m'
    green = '\033[01;32m'
    yellow = '\033[01;33m'
    cyan = '\033[01;36m'

    s = ('%' + str(length) + '.' + str(decimal_points) + 'f') % x
    if good is None:
        return s
    elif good >= 3:
        return green + s + reset
    elif good == 2:
        return cyan + s + reset
    elif good == 1:
        return yellow + s + reset
    else:
        return red + s + reset

def show(a, b, max_length=20):

    def good_enough(a, b):
        if abs(a-b) < 0.1 or abs(a-b) < a/20:
            return 3
        elif abs(a-b) < 0.2 or abs(a-b) < a/10:
            return 2
        elif abs(a-b) < 0.5 or abs(a-b) < a/4:
            return 1
        return 0

    def get_inds(inds, a):
        return map(lambda x: a[x], inds)

    if max_length > len(a):
        to_show = range(len(a))
    else:
        to_show = random.sample(range(len(a)), min(len(a), max_length) )

    show_a = get_inds(to_show, a)
    show_b = get_inds(to_show, b)
    print('-' * (len(show_a) * 8 + 7))
    print('actual: ' + ','.join(map(_to_string, show_a)))
    print('fitted: ' + ','.join(map(lambda x: _to_string(x[0], good_enough(x[1], x[0])), zip(show_b, show_a) )) )
    print('-' * (len(show_a) * 8 + 7))


def showOneOfK(a, b, doPrint=True):
    def good_enough(b):
        if b > 0.9:
            return 3
        elif b > 0.75:
            return 2
        elif b > 0.5:
            return 1
        return 0

    target = sorted(zip(a, range(len(a))))[-1][1]
    sortedB = sorted(zip(b, range(len(b))), reverse=True)
    targetFitted = filter(lambda x: x[1] == target, sortedB)[0]
    guessP = sortedB.index(targetFitted)
    if doPrint:
        print(_to_string(guessP+1, 3-guessP, 2, 0) + ' ' + _to_string(targetFitted[0], good_enough(targetFitted[0]), 1))
    return guessP

def extendStrTo(s, toLen, expansion=' '):
    if not isinstance(expansion, str) or len(expansion) == 0:
        print('expansion must have at least len=1, expansion given=' + str(expansion), file=sys.stderr)
        return s
    times = (toLen - len(s) + len(expansion) - 1) // len(expansion)
    return (s + expansion * times)[:toLen]


def printWithBorders(s, up=True, down=False, left=False, right=False):
    lines = map(lambda x: x.replace('\t', '    '), s.split('\n'))
    maxLen = max(map(len, lines)) + (left == True) + (right == True)
    if left:
        lines = map(lambda x: '|' + x, lines)
    lines = map(lambda x: extendStrTo(x, maxLen - (right == True)), lines)
    if right:
        lines = map(lambda x: x + '|', lines)
    if up:
        print('#' * maxLen)
    print('\n'.join(lines))
    if down:
        print('#' * maxLen)

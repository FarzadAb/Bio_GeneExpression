from keras.layers import containers, AutoEncoder, Dense
from keras import models
import numpy as np
import random
import pickle
import time
from represent import showOneOfK
from read import readEncData

numTests = 100
weightsFile = 'multilayer_weights.pickle'

X, y = readEncData()
numClasses = y.shape[1]
split = max( int(len(X) * .7), len(X) - numTests )

X_train = np.array(X[:split])
y_train = np.array(y[:split])

X_test = np.array(X[split:])
y_test = np.array(y[split:])

model = models.Sequential()
model.add(Dense(output_dim=40, input_dim=30, activation='sigmoid'))
model.add(Dense(40, activation='sigmoid'))
model.add(Dense(40, activation='sigmoid'))
model.add(Dense(numClasses, activation='softmax'))
model.compile(optimizer='sgd', loss='categorical_crossentropy')

# try:
#     model.set_weights(pickle.load(open(weightsFile, 'r')))
# except:
#     pass

for i in range(20):
    print('*' * 30)
    print('Starting round %d ...' %(i+1))
    start = time.time()
    model.fit(X_train, y_train, nb_epoch=200, verbose=0)
    pickle.dump(model.get_weights(), open(weightsFile, 'w'))
    print('this round took %.1f seconds' % (time.time() - start))
    rep = model.predict(X_test)
    # print(model.evaluate(X_test, y_test, show_accuracy=True))
    for j in range(min(len(X_test), 10)):
        showOneOfK(y_test[j], rep[j])

for j in range(len(X_test)):
    showOneOfK(y_test[j], rep[j])

precision = np.zeros((numClasses, numClasses))
realClass = np.zeros(numClasses)
rep = model.predict(X_test)
for i in range(len(y_test)):
    # print y_test[i], ' to ', y_test[i].tolist().index(1)
    rc = y_test[i].tolist().index(1)
    realClass[rc] += 1
    precision[rc] += rep[i]

for i, l in enumerate(precision):
    print ', '.join( map(lambda x: '%.2f' % (x / realClass[i]), l) )

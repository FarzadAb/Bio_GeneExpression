from keras.layers import containers, AutoEncoder, Dense
from keras import models
import numpy as np
import random
import pickle
import time
from represent import show
from read import readData
# from matplotlib import pylab

numTests = 100
weightsFile = 'autoencoder_weights_depriving.pickle'

X, y = readData()
removeClass = [13, 12]
X = np.array(map(lambda tup: tup[0],
                filter(lambda tup: list(tup[1]).index(1) not in removeClass,
                    zip(X,y)
                )
            ))

split = max(int(len(X) * .7), len(X) - 100)
X_train = X[:split]
X_test = X[split:]

print('len of train set is : ' + str(len(X_train)) )

autoencoder = AutoEncoder(
    encoder = containers.Sequential([
        Dense(output_dim=300, input_dim=1000, activation='sigmoid'),
        Dense(100),
        Dense(30)
    ]),
    decoder = containers.Sequential([
        Dense(output_dim=100, input_dim=30),
        Dense(300),
        Dense(1000),
    ]),
    output_reconstruction=True
)

model = models.Sequential()
model.add(autoencoder)
model.compile(optimizer='sgd', loss='mse')

# try:
#     autoencoder.set_weights(pickle.load(open(weightsFile, 'r')))
# except:
#     pass

loss = []

for i in range(300):
    print('*' * 30)
    print('Starting round %d ...' %(i+1))
    start = time.time()
    model.fit(X_train, X_train, nb_epoch=50, verbose=0)
    pickle.dump(autoencoder.get_weights(), open(weightsFile, 'w'))
    print('this round took %.1f seconds' % (time.time() - start))
    loss.append(model.evaluate(X_test, X_test, show_accuracy=False))
    print(model.evaluate(X_test, X_test, show_accuracy=True))
    print(loss)
    # rep = model.predict(X_test)
    # for i in range(len(X_test)):
    #     show(X_test[i], rep[i], 15)

# pylab.plot(loss)
# pylab.show()
f = open('loss_autoencoder_deprinving.txt', 'w')
f.write(str(loss))
f.close()

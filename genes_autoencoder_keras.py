from keras.layers import containers, AutoEncoder, Dense
from keras import models
import numpy as np
import random
import pickle
import time
import os
from represent import show
from read import readData
from matplotlib import pylab

os.environ['THEANO_FLAGS'] = 'mode=FAST_RUN,device=cpu,openmp=True,floatX=float32'
os.environ['OMP_NUM_THREADS'] = '30'

numTests = 10
weightsFile = 'autoencoder_weights.pickle'

X, _ = readData()
split = max( int(len(X) * .7), len(X) - numTests )
X_train = np.array(X[:split])
X_test = np.array(X[split:])

autoencoder = AutoEncoder(
    encoder = containers.Sequential([
        Dense(output_dim=300, input_dim=1000, activation='sigmoid'),
        Dense(100),
        Dense(30)
    ]),
    decoder = containers.Sequential([
        Dense(output_dim=100, input_dim=30),
        Dense(300),
        Dense(1000),
    ]),
    output_reconstruction=True
)

model = models.Sequential()
model.add(autoencoder)
model.compile(optimizer='sgd', loss='mse')

try:
    autoencoder.set_weights(pickle.load(open(weightsFile, 'r')))
except:
    pass

loss = []

for i in range(200):
    print('*' * 30)
    print('Starting round %d ...' %(i+1))
    start = time.time()
    model.fit(X_train, X_train, nb_epoch=50, verbose=0)
    pickle.dump(autoencoder.get_weights(), open(weightsFile, 'w'))
    print('this round took %.1f seconds' % (time.time() - start))
    loss.append(model.evaluate(X_test, X_test, show_accuracy=True))
    print(model.evaluate(X_test, X_test, show_accuracy=True))
    rep = model.predict(X_test)
    for i in range(len(X_test)):
        show(X_test[i], rep[i], 15)

pylab.plot(loss)
pylab.show()
